/* eslint-disable eol-last */
/* eslint space-before-function-paren: ["error", "never"] */

// EXAMPLE DELETE EVERYTHING BELOW
// Learn more about ES6 Here:
// http://wesbos.com/javascript-modules/
// https://babeljs.io/learn-es2015/

import $ from 'jquery';
import 'bootstrap';
import 'slick-carousel';
const colors = ['aqua', 'red', 'blue'];
const moColors = ['blue', 'orange'];

// We have Es6 Goodies thanks to Babel
const allTheColors = [...colors, ...moColors];
allTheColors.map(c => console.log(c));

// Slick carousel
$('.homeSlider').slick({
  dots: true,
  autoplay: false,
  autoplaySpeed: 4000,
  arrows: true,
  appendDots: '.home-slideshow',
  appendArrows: '.home-slideshow',
  prevArrow: '<span class="fa fa-angle-left abs-center-y prev control"></span>',
  nextArrow:
    '<span class="fa fa-angle-right abs-center-y next control"></span>',
  pauseOnHover: false
});

// Check URL for reset password hash
function checkUrlHash() {
  var hash = window.location.hash;

  if (hash === '#recover') {
    $('#RecoverPasswordForm').removeClass('d-none');
    $('#CustomerLoginForm').addClass('d-none');
  } else {
    $('#RecoverPasswordForm').addClass('d-none');
    $('#CustomerLoginForm').removeClass('d-none');
  }
}

checkUrlHash();

// Show reset password form
$('#recover').on('click', function(evt) {
  evt.preventDefault();
  $('#RecoverPasswordForm').removeClass('d-none');
  $('#CustomerLoginForm').addClass('d-none');
  window.location.hash = $(this).attr('id');
});

// Hide reset password form
$('#HideRecoverPasswordLink').on('click', function(evt) {
  evt.preventDefault();
  $('#RecoverPasswordForm').addClass('d-none');
  $('#CustomerLoginForm').removeClass('d-none');
  window.location.hash = '';
});

// Customer add address

// Check URL for reset password hash
function checkUrlAddress() {
  var hash = window.location.hash;

  if (hash === '#add-address') {
    $('#CustomerAddAddressForm').removeClass('d-none');
    $('#CustomerAddressForm').addClass('d-none');
  } else {
    $('#CustomerAddAddressForm').addClass('d-none');
    $('#CustomerAddressForm').removeClass('d-none');
  }
}

checkUrlAddress();

// Show reset password form
$('#add-address').on('click', function(evt) {
  evt.preventDefault();
  $('#CustomerAddAddressForm').removeClass('d-none');
  $('#CustomerAddressForm').addClass('d-none');
  window.location.hash = $(this).attr('id');
});

// Hide reset password form
$('#HideAddressAdder').on('click', function(evt) {
  evt.preventDefault();
  $('#CustomerAddAddressForm').addClass('d-none');
  $('#CustomerAddressForm').removeClass('d-none');
  window.location.hash = '';
});

// Cart
if ($('body').hasClass('template-cart') === true) {
  $('body').on('click', '.btn-submit', function() {
    if ($('#agree').is(':checked')) {
      $(this).submit();
    } else {
      $('.summary .alert').removeClass('d-none');
      return false;
    }
  });
  $('body').on('click', '.custom-checkbox', function() {
    $('.summary .alert').addClass('d-none');
  });
}
